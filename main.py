class Game:
    def __init__(self):
        self.field = [['W', ' ', 'W', ' ', 'W', ' ', 'W', ' '],
                      [' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W'],
                      [' ', ' ', 'W', ' ', ' ', ' ', 'W', ' '],
                      [' ', 'W', ' ', 'B', ' ', 'W', ' ', ' '],
                      [' ', ' ', 'B', ' ', 'B', ' ', ' ', ' '],
                      [' ', ' ', ' ', ' ', ' ', ' ', ' ', 'B'],
                      ['B', ' ', 'B', ' ', 'B', ' ', 'B', ' '],
                      [' ', 'B', ' ', 'B', ' ', ' ', ' ', 'B']]
        self.turn = 'W'

    def print_field(self):
        print(' {} '.format(self.turn) + ' '.join(['1', '2', '3', '4', '5', '6', '7', '8']) + ' ')
        pre = list(map(lambda x: '|' + '|'.join(x) + '|', self.field))
        for i in range(8):
            print(alph[i].upper(), pre[i])

    def turn_switch(self):
        if self.turn == 'W':
            self.turn = 'B'
        else:
            self.turn = 'W'

    def move(self, x1, y1, x2, y2):
        y1 = alph.index(y1.lower())
        y2 = alph.index(y2.lower())
        x1 = int(x1) - 1
        x2 = int(x2) - 1
        if self.field[y1][x1][0] == self.turn:
            if (self.field[y1][x1][-1] == 'Q') or ((abs(x2 - x1) == 1) and (abs(y2 - y1) == 1)):
                if (y2 == 7) and (self.turn == 'W'):
                    self.field[y2][x2] = 'WQ'
                    self.field[y1][x1] = ' '
                elif (y2 == 0) and (self.turn == 'B'):
                    self.field[y2][x2] = 'BQ'
                    self.field[y1][x1] = ' '
                else:
                    self.field[y2][x2] = self.field[y1][x1]
                    self.field[y1][x1] = ' '
                self.turn_switch()
                self.print_field()
            else:
                print('Incorrect move')
        else:
            print('Its not your turn')

    def eat(self, x1, y1, *next_move):
        x1 = int(x1) - 1
        y1 = alph.index(y1.lower())
        for i in next_move:
            x2 = int(i[0]) - 1
            y2 = alph.index(i[1].lower())
            if self.field[y1][x1][0] == self.turn:
                if (self.field[y1][x1][-1] == 'Q') or ((abs(x2 - x1) == 1) and (abs(y2 - y1) == 1)):
                    if self.field[y2 + (y2 - y1)][x2 + (x2 - x1)] == ' ':
                        self.field[y2][x2] = ' '
                        self.field[y2 + (y2 - y1)][x2 + (x2 - x1)] = self.field[y1][x1]
                        self.field[y1][x1] = ' '
                        if (y2 + (y2 - y1) == 7) and (self.turn == 'W'):
                            self.field[y2 + (y2 - y1)][x2 + (x2 - x1)] = 'WQ'
                        elif (y2 + (y2 - y1) == 0) and (self.turn == 'B'):
                            self.field[y2 + (y2 - y1)][x2 + (x2 - x1)] = 'BQ'
                    x1 = x2 + (x2 - x1)
                    y1 = y2 + (y2 - y1)
                else:
                    print('Incorrect move')
            else:
                print('Its not your turn')
        self.turn_switch()
        self.print_field()


alph = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
g = Game()
g.print_field()
while True:
    a = input('Сделайте свой ход(move, eat)\n').rstrip(' ')
    if a.lower() == 'eat':
        b = input().split()
        c = list(map(str.split, input().split(',')))
        g.eat(*b, *c)
    elif len(a.split()) == 4:
        g.move(*a.split())
    else:
        print('Wrong command\n')
